clc,clear;
% Determine airfoil angle of attack
    alpha_deg=4;
    alpha = 2*pi*alpha_deg/360;  % Angle of attack (in radians)
    transM=[[cos(alpha),sin(alpha)];[-sin(alpha),cos(alpha)]];
    % Define native NACA0012 PARSEC paramteters 
p=[0.0146,0.3025,0.06,-0.4928,0.3016,0.06,-0.4848,-0.0039,0.0101,-2.7791,9.2496];


% Mesh dimensions
    scale = 1 ;           % Scaling factor
    H = 8     ;           % *Half* height of channel
    W = 0.5   ;           % *Half* depth of foil (y-direction)
    D = 16    ;           % Length of downstream section
% Mesh resolution parameters
    Nx = 250    ;% Number of mesh cells along the foil
    ND = 150    ;% Number of cells in the downstream direction
    NT = 100    ;% Number of cells the transverse direction
    NW = 1      ;% Number of cells in the y-direction (along the foil axis)
% Expansion rates
    ExpT = 500       ;    % Expansion rate in transverse direction
    ExpD = 100       ;    % Expansion rate in the downstream direction
    ExpArc = 50      ;    % Expansion rate along the inlet arc
% --------------------- END OF MESH PARAMETER REGION ----------------- #

% Compute shape coefficients
a = parsec(p);
point=1000;
n=(1/point);
% Enable higher resolution at LE
nn=n/5; 
% suction surface coordinates
x1=1:-n:0.1;
x2=0.1:-nn:0;
% pressure surface coordinates
x3=0:nn:0.1;
x4=0.1:n:1;

xu(1:length(x1))=x1;
xu(length(x1)+1:length(x1)+length(x2))=x2;

xl(1:length(x3))=x3;
xl(length(x3)+1:length(x3)+length(x4))=x4;
xl = flip(xl);
Ni = length(xl)    ;% Number of interpolation points along the foil

yu=a(1)*xu.^.5+a(2)*xu.^(1.5)+a(3)*xu.^(2.5)+a(4)*xu.^(3.5)+a(5)*xu.^(4.5)+a(6)*xu.^(5.5);
yl=-(a(7)*xl.^.5+a(8)*xl.^(1.5)+a(9)*xl.^(2.5)+a(10)*xl.^(3.5)+a(11)*xl.^(4.5)+a(12)*xl.^(5.5));
yl = flip(yl);
%Calculate camber and thickness
y_c = (yu+yl)/2;
y_t = (yu-yl);
% find index of maximum camber
I = 0;
    if y_c == zeros(length(xl))    % when no camber, use max thickness
     [z,I] = max(y_t);
    else
     [z,I] = max(y_c);      
            end
    U=transM*[xu;yu];
    L=transM*[xl;yl];
    Xu=U(1,:);
    Zu=U(2,:);
    Xl=L(1,:);
    Zl=L(2,:);
% Move point of mesh "nose"
    NoseX = (-H +Xu(I))*cos(alpha);
    NoseZ = -(-H + Xu(I))*sin(alpha);
%Calculate the location of the vertices on the positive
%y-axis and put them in a matrix
    V=zeros(12,3);
    V(1,:) = [NoseX,W,NoseZ];
    V(2,:) = [Xu(I),W,H];
    V(3,:) = [Xu(Ni),W,H];
    V(4,:) = [D,W,H];
    V(5,:) = [Xu(1),W,Zu(1)];
    V(6,:) = [Xu(I),W,Zu(I)];
    V(7,:) = [Xl(I),W,Zl(I)];
    V(8,:) = [Xu(Ni),W,Zu(Ni)];
    V(9,:) = [D,W,Zu(Ni)];
    V(10,:) = [Xl(I),W,-H];
    V(11,:) = [Xu(Ni),W,-H];
    V(12,:) = [D,W,-H];
%Create vertices for other side (negative y-axis)
    V2=[V(:,1),V(:,2)*1-1,V(:,3)];
    V=[V;V2];
%Edge 4-5 and 16-17 
    pts1 = [Xu(2:I)', W*ones(I-1,1),Zu(2:I)'];
    pts5 = [pts1(:,1), -pts1(:,2), pts1(:, 3)];
%Edge 5-7 and 17-19
    pts2 = [Xu(I+1:Ni)',W*ones((Ni-I),1),Zu(I+1:Ni)'];
    pts6 = [pts2(:,1), -pts2(:,2), pts2(:, 3)];
%Edge 4-6 and 16-18
    pts3 = [Xl(2:I)',W*ones(I-1,1),Zl(2:I)'];
    pts7 =  [pts3(:,1), -pts3(:,2), pts3(:, 3)];
%Edge 6-7 and 18-19
    pts4 = [Xl(I+1:Ni)',W*ones(Ni-I,1),Zl(I+1:Ni)'];
    pts8 =  [pts4(:,1), -pts4(:,2), pts4(:, 3)];
%Edge 0-1 and 12-13
    pts9 = [(-H*cos(pi/4))+Xu(I),W,H*sin(pi/4)];
    pts11 =  [pts9(1), -pts9(2), pts9(3)];
%Edge 0-9 and 12-21
    pts10 = [(-H*cos(pi/4))+Xu(I),W,-H*sin(pi/4)];
    pts12 =  [pts10(1), -pts10(2), pts10(3)];
%Calculate number of mesh points along 4-5 and 4-6    
    Nleading = int64((I/Ni)*Nx);
    %Nleading = x(C_max_idx)/c*Nx(?)

    % Calculate number of mesh points along 5-7 and 6-7
    Ntrailing = Nx - Nleading;
    
    
    % Write File
    fileID = fopen('BlockMesh.dict','w+');
    fprintf(fileID,"/*--------------------------------*- C++ -*----------------------------------*\\ \r\n");
    fprintf(fileID,"| =========                 |                                                 | \r\n");
    fprintf(fileID,"| \\\\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox           | \r\n");
    fprintf(fileID,"|  \\\\    /   O peration     | Version:  3.0.x                                 | \r\n");
    fprintf(fileID,"|   \\\\  /    A nd           | Web:      www.OpenFOAM.com                      | \r\n");
    fprintf(fileID,"|    \\\\/     M anipulation  |                                                 | \r\n");
    fprintf(fileID,"\\*---------------------------------------------------------------------------*/ \r\n");
    fprintf(fileID,"FoamFile                                                                        \r\n");
    fprintf(fileID,"{                                                                               \r\n");
    fprintf(fileID,"    version     2.0;                                                            \r\n");
    fprintf(fileID,"    format      ascii;                                                          \r\n");
    fprintf(fileID,"    class       dictionary;                                                     \r\n");
    fprintf(fileID,"    object      blockMeshDict;                                                  \r\n");
    fprintf(fileID,"}                                                                               \r\n");
    fprintf(fileID,"// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * // \r\n");
    fprintf(fileID,"\r\n")
    fprintf(fileID,"convertToMeters %f; \r\n" ,scale);
    fprintf(fileID,"\r\n")
    fprintf(fileID,"vertices \r\n");
    fprintf(fileID,"( \r\n");
    formatSpec_vertices = '(%f,%f,%f)\r\n' ;
    fprintf(fileID,formatSpec_vertices,V');
    fprintf(fileID,"); \r\n");
    fprintf(fileID,"\r\n");
    fprintf(fileID,"blocks \r\n");
    fprintf(fileID,"( \r\n");
    fprintf(fileID,"    hex (4 5 1 0 16 17 13 12)     (%i %i %i) edgeGrading (1 %f %f 1 %f %f %f %f 1 1 1 1) \r\n" ,Nleading, NT, NW, 1/ExpArc, 1/ExpArc, ExpT, ExpT, ExpT, ExpT);
    fprintf(fileID,"    hex (5 7 2 1 17 19 14 13)     (%i %i %i) simpleGrading (1 %f 1) \r\n" ,Ntrailing, NT, NW, ExpT);
    fprintf(fileID,"    hex (7 8 3 2 19 20 15 14)     (%i %i %i) simpleGrading (%f %f 1) \r\n" ,ND, NT, NW, ExpD, ExpT);
    fprintf(fileID,"    hex (16 18 21 12 4 6 9 0)     (%i %i %i) edgeGrading (1 %f %f 1 %f %f %f %f 1 1 1 1) \r\n" ,Nleading, NT, NW, 1/ExpArc, 1/ExpArc, ExpT, ExpT, ExpT, ExpT);
    fprintf(fileID,"    hex (18 19 22 21 6 7 10 9)    (%i %i %i) simpleGrading (1 %f 1) \r\n" ,Ntrailing, NT, NW, ExpT);
    fprintf(fileID,"    hex (19 20 23 22 7 8 11 10)   (%i %i %i) simpleGrading (%f %f 1) \r\n" ,ND, NT, NW, ExpD, ExpT);
    fprintf(fileID,"); \r\n");
    fprintf(fileID,"\r\n");
    fprintf(fileID,"edges \r\n");
    fprintf(fileID,"( \r\n");

    fprintf(fileID,"    spline 4 5 \r\n");
    fprintf(fileID,"        ( \r\n");
    fprintf(fileID,"            (%f %f %f) \r\n" , pts1');
    fprintf(fileID,"        ) \r\n");
    fprintf(fileID,"    spline 5 7 \r\n");
    fprintf(fileID,"        ( \r\n");
        fprintf(fileID,"            (%f %f %f)\r\n" ,pts2');
    fprintf(fileID,"        ) \r\n");
    fprintf(fileID,"    spline 4 6 \r\n");
    fprintf(fileID,"        ( \r\n");
        fprintf(fileID,"            (%f %f %f)\r\n" ,pts3');
    fprintf(fileID,"        ) \r\n");
    fprintf(fileID,"    spline 6 7 \r\n");
    fprintf(fileID,"        ( \r\n");
        fprintf(fileID,"            (%f %f %f)\r\n" ,pts4');
    fprintf(fileID,"        ) \r\n");
    fprintf(fileID,"    spline 16 17 \r\n");
    fprintf(fileID,"        ( \r\n");
        fprintf(fileID,"            (%f %f %f)\r\n" ,pts5');
    fprintf(fileID,"        ) \r\n");
    fprintf(fileID,"    spline 17 19 \r\n");
    fprintf(fileID,"        ( \r\n");
        fprintf(fileID,"            (%f %f %f)\r\n" ,pts6');
    fprintf(fileID,"        ) \r\n");
    fprintf(fileID,"    spline 16 18 \r\n");
    fprintf(fileID,"        ( \r\n");
        fprintf(fileID,"            (%f %f %f)\r\n" ,pts7');
    fprintf(fileID,"        ) \r\n");
    fprintf(fileID,"    spline 18 19 \r\n");
    fprintf(fileID,"        ( \r\n");
        fprintf(fileID,"            (%f %f %f)\r\n" ,pts8');
    fprintf(fileID,"        ) \r\n");
    fprintf(fileID,"    arc 0 1 (%f %f %f) \r\n" ,pts9');
    fprintf(fileID,"    arc 0 9 (%f %f %f) \r\n" ,pts10');
    fprintf(fileID,"    arc 12 13 (%f %f %f) \r\n" ,pts11');
    fprintf(fileID,"    arc 12 21 (%f %f %f) \r\n" ,pts12');
    fprintf(fileID,"); \r\n");
    fprintf(fileID,"\r\n");
    fprintf(fileID,"boundary \r\n");
    fprintf(fileID,"( \r\n");
    fprintf(fileID,"    inlet \r\n");
    fprintf(fileID,"    { \r\n");
    fprintf(fileID,"        type patch; \r\n");
    fprintf(fileID,"        faces \r\n");
    fprintf(fileID,"        ( \r\n");
    fprintf(fileID,"            (1 0 12 13) \r\n");
    fprintf(fileID,"            (0 9 21 12) \r\n");
    fprintf(fileID,"        ); \r\n");
    fprintf(fileID,"    } \r\n");
    fprintf(fileID,"\r\n");
    fprintf(fileID,"    outlet \r\n");
    fprintf(fileID,"    { \r\n");
    fprintf(fileID,"        type patch; \r\n");
    fprintf(fileID,"        faces \r\n");
    fprintf(fileID,"        ( \r\n");
    fprintf(fileID,"            (11 8 20 23) \r\n");
    fprintf(fileID,"            (8 3 15 20) \r\n");
    fprintf(fileID,"        ); \r\n");
    fprintf(fileID,"    } \r\n");
    fprintf(fileID,"\r\n");
    fprintf(fileID,"    topAndBottom \r\n");
    fprintf(fileID,"    { \r\n");
    fprintf(fileID,"        type patch; \r\n");
    fprintf(fileID,"        faces \r\n");
    fprintf(fileID,"        ( \r\n");
    fprintf(fileID,"            (3 2 14 15) \r\n");
    fprintf(fileID,"            (2 1 13 14) \r\n");
    fprintf(fileID,"            (9 10 22 21) \r\n");
    fprintf(fileID,"            (10 11 23 22) \r\n");
    fprintf(fileID,"        ); \r\n");
    fprintf(fileID,"    } \r\n");
    fprintf(fileID,"\r\n");

    fprintf(fileID,"    airfoil \r\n");
    fprintf(fileID,"    { \r\n");
    fprintf(fileID,"        type wall; \r\n");
    fprintf(fileID,"        faces \r\n");
    fprintf(fileID,"        ( \r\n");
    fprintf(fileID,"            (5 4 16 17) \r\n");
    fprintf(fileID,"            (7 5 17 19) \r\n");
    fprintf(fileID,"            (4 6 18 16) \r\n");
    fprintf(fileID,"            (6 7 19 18) \r\n");
    fprintf(fileID,"        ); \r\n");
    fprintf(fileID,"    } \r\n");
    fprintf(fileID,"); \r\n");
    fprintf(fileID," \r\n");
    fprintf(fileID,"mergePatchPairs \r\n");
    fprintf(fileID,"( \r\n");
    fprintf(fileID,"); \r\n");
    fprintf(fileID," \r\n");
    fprintf(fileID,"// ************************************************************************* // \r\n");
    fclose(fileID);
    
    
    

       


    
    
    
    